#Base configuration for MOBOTIX cameras

I created (and try to maintain) this set of configuration files to speed up deployment of MOBOTIX systems as well as improving the base security of the cameras.

To use them, download the relevant files and upload them to your cameras before making other changes. Upload all sections except network (set this to suit your site).

See below a full list of changes made from the factory default configuration

##Admin menu

| Section | Settings | Factory | Change |
| ------- | -------- | ------- | ------ |
| Quick Install | | | Skipped |
| Group Access Control Lists | Public Access | Available | Disabled for all |
| | User group access to menus | Image Setup & Event Setup | Remove menu access |
| Language & Start Page | Web crawler restriction | Enabled | Disabled |
| Web Server | Enable intrusion detection | Disabled | Enabled |
| | Notification threshold | | 5 |
| | Timeout | | 60 |
| | Deadtime | | 60 |
| | Block IP address | Disabled | Enabled |
| | E-mail notification | None | IntrustionMail |
| SNMP Configuration | Agent | None | SNMP v2 Only |
| | Read only community string | None | public |
| Storage Failure Detection | E-mail | None | StorageFailureDetection |
| | Frequency of e-mail | Once | Every 24 hours |
| | Additional information | Unchecked | Checked |
| | Startup delay | none | 15 minutes |
| Logo profiles | | | All factory profiles deleted |
| FTP profiles | | | All factory profiles and details deleted |
| IP Notify profiles | | | All factory profiles deleted |
| E-mail profiles | | | All factory profiles deleted |
| | | | Profiles created as outlined below |
| Speaker & Microphone | Announce network data | Enabled | Disabled |
| Sound profiles | | | All factory profiles deleted |
| Time & Date | Time Servers | none | NTP `0.pool.ntp.org`, `1.pool.ntp.org`, `2.pool.ntp.org` |
| LED Setup | Main switch | Default Configuration | Disabled |
| Time Tables | | | All factory profiles deleted |

### E-mail Profiles

**PROFILE: StorageFailureMail**
Camera: $(ID.NAM)
IP address: $(ID.ET0)
Serial Number: $(ID.FIP)
Firmware Version: $(ID.SWV)
Time since last reboot: $(ID.UPT)

Buffer (current): $(STORAGE.BUFFERFILL.CURRENT)%
Buffer (average): $(STORAGE.BUFFERFILL.AVERAGE)%
Buffer (maximum): $(STORAGE.BUFFERFILL.MAXIMUM)%

Transfer delay (current): $(STORAGE.TRANSFERDELAY.CURRENT)
Transfer delay (average): $(STORAGE.TRANSFERDELAY.AVERAGE)
Transfer delay (maximum): $(STORAGE.TRANSFERDELAY.MAXIMUM)

- Include System messages for past 12 hours
- Subject: Storage failure on $(ID.NAM)


**PROFILE: IntrusionMail**
Attempted login attempts on
Camera: $(ID.NAM)
IP address: $(ID.ET0)

Check attached log file for information

- Include web server log file
- Subject: Intrusion alarm on $(ID.NAM)

##Setup Menu

| Section | Settings | Factory | Change |
| ------- | -------- | ------- | ------ |
| General Image Settings | Display mode | varies by model | Full Image |
| Exposure Settings | Maximum exposure time | 1/5 | 1/30 |
| Text & Display Settings | | | Add $(ID.NAM) to the text display |
| Event Settings | | | Delete all but UC, VM, and AS |
| Action Groups | | | All factory profiles deleted |
| Recording | Recording status symbol | On | Off |
| | Audio recording | Enabled | Disabled |
| | Retrigger recording | Select None | Select All |
| | Pre-event recording | 0s | 5s |
| vPTZ | | | Lock |
| General Arming | Arming | Disabled | Enabled |

##Display Pages

| Page | Change |
| ---- | ------ |
| Live Page | Remove all but Admin Menu, Setup Menu, UC, Event List, Multiwatcher, 1x Zoom, Zoom +, Zoom -, and Center Pan softbuttons |
| Event Player Page | Remove all but Event List and Multiwatcher softbuttons |
| | Add in Event Download softbutton |
| MultiView Page | Remove all but UC, Event List, Multiwatcher, 1x Zoom, Zoom +, Zoom -, and Center Pan softbuttons |

##Things you still need to do

- Remember to enable IP Level Access Control and add in your relevant settings
- Set time zone and root cameras server
- Set storage server target
- Configure network
- Configure recording triggers/type
- Configure events and actions
